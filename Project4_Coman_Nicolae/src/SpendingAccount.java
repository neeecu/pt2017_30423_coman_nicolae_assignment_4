// card de debit ; n-are dobanda // socti / pui bani cand ai chef
public class SpendingAccount extends Account implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final double transactionFee=0.00002;
	
	public SpendingAccount(int ID, double Balance){
		super(ID,Balance);
	}
	
	public SpendingAccount(){super();} 
	
	public SpendingAccount(double balance)
	{
		super(balance);
	}
	
	public void withdraw(double sum){
		
		balance=balance - ( (balance*transactionFee) +sum);
		setChanged();
		notifyObservers();
	}
	
	public void deposit (double sum)
	{
		
		balance = (balance- (balance*transactionFee)) + sum;
		setChanged();
		notifyObservers();
		
	}
	
	
	public String toString() {
		return "SpendingAccount [" + balance + ", " + accountID + "]";
	}
	}
