import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ErrorFrame {

	
	
	public static void runErrorFrame(String ErrorString)
	{
		JFrame mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel mainPanel = new JPanel();
		
		JTextField errorTextField = new JTextField();
		errorTextField.setText(ErrorString);
		errorTextField.setEditable(false);
		
		mainPanel.add(errorTextField);
		
		mainFrame.add(mainPanel);
		mainFrame.pack();
		mainFrame.setVisible(true);
		
		
		
	}
}
