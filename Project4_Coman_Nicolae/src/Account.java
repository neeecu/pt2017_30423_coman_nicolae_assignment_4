import java.util.Observable;

public class Account extends Observable implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public long accountID;
	public double balance;
	 
	
	public Account(){}
	
	public Account (long accountID, double balance)
	{
		
		this.accountID=accountID;
		this.balance=balance;
	}
	
	public Account (double balance)
	{
		this.balance=balance;
	}
	
	
	public void setID(long ID)
	{
		this.accountID=ID;
	}
	
	public long getID()
	{
		return this.accountID;
	}
	
	public double getBalance()
	{
		
		return this.balance;
	}
	
	 
	public void deposit(double sum) throws Exception
	{
		this.balance+=sum;
		setChanged();
		notifyObservers();
	}
	
	public void withdraw(double sum)
	{
		
		this.balance-=sum;
		setChanged();
		notifyObservers();
	}
}
