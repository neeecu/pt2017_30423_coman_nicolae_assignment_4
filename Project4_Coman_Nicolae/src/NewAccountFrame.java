import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
 

public class NewAccountFrame {
	
	
	public static JComboBox<Person> personComboBox = new JComboBox<Person>();

	
	public static void NewAccountFrame()
	{
		
		  App.banca.fillComboBox();
		  
		  JComboBox<String> accountType= new JComboBox<String>();
		  
		  JButton Add = new JButton("Add");
		  
		  
		 
		  
		  accountType.addItem("SavingAccount");
		  accountType.addItem("SpendingAccount");
		  
		  
		  JFrame MainFrame= new JFrame("New Account");
		  MainFrame.setSize(300,300);
		  
		  JPanel MainPanel = new JPanel();
		  MainPanel.add(personComboBox);
		  MainPanel.add(accountType);
		
		  MainPanel.add(Add);
		  
		  MainFrame.add(MainPanel);
		  MainFrame.setVisible(true);
		  
		  class  Handler  implements ActionListener 
	      {  
	           
	    	  public void actionPerformed(ActionEvent e){
	    		  
	    		  
	    		  if(e.getSource()==Add)
	    		  {
	    			  
	    			  Account newAcc =null ;
	    			  
	    			  Person customer =(Person) NewAccountFrame.personComboBox.getSelectedItem();
	    			  
	    			  String type = (String) accountType.getSelectedItem();
	    			  
 	    			  
	    			  if(type.equals("SavingAccount"))
	    				  newAcc= new SavingAccount();
	    			  
	    			  if(type.equals("SpendingAccount"))
	    			  {
	    				
	    				  newAcc= new SpendingAccount();
	    				  
	    			  }
	    			  
	    			  AccountGUI.accountModel.setRowCount(0);

	    			  App.banca.addAcount(customer,newAcc);
	    			  MainFrame.dispose();
	    			  AccountGUI.mainFrame.setVisible(true);
	    			  App.banca.viewAllAccounts();
	    			  
	    			  
	    		  }
	    	  }
		
	}
	
		  Handler handler = new Handler();
		  Add.addActionListener(handler);
}
}
