
public interface BankProc {

	/*
	 * Precondition : customer!=NULL
	 * Precondition : acc !=NULL
	 * Postcondition: table.get(customer) ++;  - set containg the accounts of the customer increases its size
	 * */
	public long addAcount(Person customer, Account acc);
	
	
	
	/*
	 *  Precondition:AccountId >0  
	 *  
	 *  Postcondtiion: table.get(customer) --
	 * */
	public void removeAccount(long AccountID);
	
	
	
	
	/*
	 * 
	 * 
	 * */
	public void editAccountData (long AccountID, Account toWrite);
	
	
	
	
	/*
	 * 	Precondition : there are entries in the bank,i.e., bank.table =NULL && bank.table.KeySet()!=nuLL
	 * 
	 * 
	 * */
	public void viewAllAccounts();
	
	
	
	/*
	 * Precondition: PersonID >0  
	 * Postcondition:bankPersons.size()--
	 * 
	 * 
	 * */
	public void removePerson(long PersonID);
	
	
	
	/*
	 *  Precondition : customer!=NULL
	 *  Postcondition : bankPersons.size()++;
	 * 
	 * */
	public long addPerson (Person customer);
	
	
	
	
	/*
	 * 
	 * Precondition:personID>0
	 * 				toWrite!=NULL
	 * Postcondition:PersonDI=toWrite.getID();
	 * */
	public void editPersonData(long PersonID, Person toWrite);
	
	
	
	
	/*
	 * 	Preconditon bankPersons!=null
	 * 
	 * 
	 * 
	 * */
	public void viewAllPersons();
	
	
	
	
	/*there must be persons in the bank in order to view any data
	 * BankPersons.size()>=1 
	 * 
	 * */
	public void generateReport();
	
 
	
}
