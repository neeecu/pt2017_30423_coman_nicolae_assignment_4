import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.awt.Desktop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
 
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

 

public class Bank implements BankProc,java.io.Serializable {
	
	  
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long lastAccountID=0;
	private long lastPersonID=0;
	
	  Hashtable<Person,Set<Account>> table = new Hashtable<Person,Set<Account>>();
	
	//always add new persons first to the set, then only after that add an account for them and make entry in table
	  Set<Person> bankPersons = new TreeSet<Person>();
	
	public Bank () {}
	
	public long addAcount(Person customer, Account acc){	
		lastAccountID++;
		acc.addObserver(customer);

		acc.setID(lastAccountID);
		assert customer!=null :"Precondition customer must not be null";
		assert acc!=null : "Precondition accout must not be null";
		assert validate()==true: "The data structures cannot be null";
		int size1=0,size2=0;
		
		if(checkAccount(customer)==true){
			Set <Account> toAdd = new HashSet<Account>();
			toAdd.add(acc);
			 size1=0;
			this.table.put(customer, toAdd);
			 
				size2= this.table.get(customer).size();
		}
		
		else{
			size1=this.table.get(customer).size();
			this.table.get(customer).add(acc);
			size2=this.table.get(customer).size();
		}
	   assert size2==size1+1 :size2;
		
	   return lastAccountID;
	}

	public void removeAccount(long AccountID) {
		int size1=0,size2=0; 
		Set <Person> theSet= this.table.keySet();
		assert validate()==true: "The data structures cannot be null";
		if(theSet.size()==0){
			throw new NullPointerException("The table is empty");
		}
		
		assert AccountID>0 & AccountID<=lastAccountID :"Precondition: Account ID not in the correct range";
		
		for(Person a:theSet){
			for(Iterator<Account> iterator=this.table.get(a).iterator();iterator.hasNext();){
					Account toDel =iterator.next();
					if(toDel.getID()==AccountID){
						size1=this.table.get(a).size();
						iterator.remove();
						size2=this.table.get(a).size();
					}
				}		
		
		}
		assert size2==size1-1: "Postconditon : The size of the set containing the accounts must be smaller after deletion";
	}
	
	 
	public void editAccountData(long AccountID, Account toWrite) {
		Set <Person> theSet= this.table.keySet();
		assert validate()==true: "The data structures cannot be null";

		if(theSet.size()==0){
			throw new NullPointerException("The table is empty");
		}
		
		assert AccountID>0 & AccountID<=lastAccountID :"Precondition: Account ID not in the correct range";

		for(Person a:theSet)
		{
			
			for(Iterator<Account> iterator=this.table.get(a).iterator();iterator.hasNext();){
					Account toModify =iterator.next();
					if(toModify.getID()==AccountID){
 						iterator.remove();
						toWrite.setID(AccountID);
					    toWrite.addObserver(a);
						this.table.get(a).add(toWrite);
						 
					}
				}
		}
		
	}
	@Override
	public long addPerson(Person customer) {
		assert customer!=null:"The customer cannot be null";
		assert validate()==true: "The data structures cannot be null";
		int size1= bankPersons.size();
		lastPersonID++;
		customer.setID(lastPersonID);
		bankPersons.add(customer);
		int size2= bankPersons.size();
		assert size2==size1+1:"The person is not visible in the bank";
		return lastPersonID;	
	}
	
	public void removePerson(long PersonID) {
		assert PersonID>0 :"The PersonID to be removed must be greater than zero";
		assert validate()==true: "The data structures cannot be null";
		Enumeration<Person> it=this.table.keys();
		int size1=bankPersons.size();
		int size2=0;
		while(it.hasMoreElements()){	
			 Person a =it.nextElement();
			if(a.getID()==PersonID){
				this.table.remove(a);		
			}
			
		}
		for(Iterator<Person> iterator =this.bankPersons.iterator() ;iterator.hasNext();){
			Person toDel=iterator.next();
			if(PersonID==toDel.getID()){
				iterator.remove();
				size2= bankPersons.size();
			}
			
		}
		assert size1==size2+1 :"The Given person was not removeed from the bak";
	}

	
	public void editPersonData(long PersonID, Person toWrite){
		assert PersonID >0:"PersonID to be edited must be greater than zero";
		assert toWrite!=null:"The Person to be Edited cannot be null";
		assert validate()==true: "The data structures cannot be null";
		Set <Person> theSet= this.table.keySet();
		Set <Account> saveAcc = new HashSet<Account>();
		Person Aux = new Person();
		Person Aux2= new Person();
		toWrite.setID(PersonID);
		for(Person a:this.bankPersons)
		{
			if(a.getID()==PersonID){
				Aux2=a;
			}
			
		}
		this.bankPersons.remove(Aux2);
		this.bankPersons.add(toWrite);
		for(Person a:theSet){
			
			 if(a.getID()==PersonID){
				 Aux=a;
				 saveAcc=this.table.get(a);
			 }
		}
		
		assert PersonID == toWrite.getID():"ID mismathc when editing a person";
		this.table.remove(Aux);
		this.table.put(toWrite,saveAcc);
				
	}

	public void viewAllPersons(){
		assert bankPersons!=null && bankPersons.size()>0 :"There are no persons in the bank";
		assert validate()==true: "The data structures cannot be null";
		for(Person a : bankPersons){
			PersonGUI.personModel.addRow(new Object[]{a.getID(),a.getFirstName(),a.getLastName(),a.getAddresss()});
		}
		
	}
	 
	public void viewAllAccounts() {
		 Set <Person> theSet = this.table.keySet();
		 assert theSet.size()>0 :"There are no persons in the bank, so there are no accounts";
    	 assert validate()==true: "The data structures cannot be null";
		 for(Person a : theSet) {
			 for(Iterator<Account> iterator=this.table.get(a).iterator();iterator.hasNext();) {
				 Account now = iterator.next();
			 
				 AccountGUI.accountModel.addRow(new Object[]{now.getID(),now.getBalance()+" $",now.getClass().getName(),a.getFirstName()+" "+a.getLastName()});
			 }
		 }	
	}

	public Person findByID(int ID){
		Person toReturn = new Person();
		assert validate()==true: "The data structures cannot be null";
		for(Person a: this.bankPersons){
			if(a.getID()==ID){
				toReturn.setFirstName(a.getFirstName());
				toReturn.setLastName(a.getLastName());
				toReturn.setID(ID);
				toReturn.setAddresss(a.getAddresss());
				return toReturn;
			}
		}
		return null;
	}
	
	public boolean checkAccount(Person person){
		if(this.table.get(person)==null)
			return true;
		else 
			return false;	
	}
	
	public void fillComboBox(){
		for(Person a: this.bankPersons){
			NewAccountFrame.personComboBox.addItem(a);
			 
		}
		
	}

	public void fillAccountComboBox(int ComboBox){
		Set<Person> bankPersons = this.table.keySet();
		for(Person a : bankPersons){
			for(Iterator<Account> iterator=this.table.get(a).iterator();iterator.hasNext();){
				if(ComboBox==0)
					WithdrawFrame.Accounts.addItem(iterator.next());
				if(ComboBox==1)
					DepositFrame.Accounts.addItem(iterator.next()); 
			 }	
		}	
	}
 
	public  void fillList(){
		Set<Person> bankPersons = this.table.keySet();
		for(Person a : bankPersons){
			for(Iterator<Account> iterator=this.table.get(a).iterator();iterator.hasNext();){
		
					AccountGUI.lista.addElement( iterator.next());
			 }
		}
		
	}
	
	@Override
	public void generateReport() {
		assert bankPersons.size()>=1 :"The bank is empty";
 		  File file2=new File("C:\\Users\\neecu\\workspace\\Project4_Coman_Nicolae\\Report.pdf");

	try{  
		if(this!=null)	{
		OutputStream file = new FileOutputStream(file2);
		Document document = new Document(); 
		PdfWriter.getInstance(document, file);
		document.open(); 
		for(Person a : bankPersons){
			document.add(new Paragraph("\n"+a.toString()));
			if(this.table.get(a)!=null){
				for(Iterator<Account> iterator=this.table.get(a).iterator();iterator.hasNext();){
		
					document.add(new Paragraph("\n"+"\t"+"\t"+iterator.next().toString()));
				}
			}
			else continue;
		}
		document.addTitle(" BankReport");
		document.close();
		file.close();
		if (Desktop.isDesktopSupported())
        	Desktop.getDesktop().open(file2);

		}
		 
	}
	catch(Exception excep){	
	 	excep.printStackTrace();
		}
	}
	 
	private boolean validate()
	{
		boolean value = true;
		if (this.bankPersons== null || this.table==null)
			value =false;
		
		return value;
	}
	

	public static Bank deserialize()
	{
		Bank newBank= new Bank();
		 try {
	         FileInputStream fileIn = new FileInputStream("banca.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         newBank = (Bank) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i) {
	         i.printStackTrace();
	         return null;
	      }catch(ClassNotFoundException c) {
	         System.out.println("Employee class not found");
	         c.printStackTrace();
	         return null;
	      }
		
		return newBank;
		
	}
	
	
}