import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class DepositFrame {
	
	
	public static JComboBox<Account> Accounts = new JComboBox<Account>();

	public static void RunDepositFrame()
	{
	JFrame DepositMainFrame = new JFrame();
	DepositMainFrame.setSize(300,300);
	
	JPanel mainPanel = new JPanel();
	
	
	
	
	
	JButton Deposit= new JButton("Deposit");
	JPanel buttonJPane = new JPanel();
	buttonJPane.setLayout(new FlowLayout());
	buttonJPane.add(Deposit);
	
	
	JTextField balance = new JTextField();
	JLabel label1= new JLabel("Amount");
	balance.setColumns(10);
	
	Accounts.removeAllItems();
	App.banca.fillAccountComboBox(1);

	
	
	JPanel ComboBoxPanel = new JPanel();
	ComboBoxPanel.setLayout(new FlowLayout());
	
	JPanel balanceTextFieldPanel = new JPanel();
	balanceTextFieldPanel.setLayout(new FlowLayout());
	balanceTextFieldPanel.add(label1);
	balanceTextFieldPanel.add(balance);
	
	
	
	ComboBoxPanel.add(Accounts);
	mainPanel.add(ComboBoxPanel);
	mainPanel.add(balanceTextFieldPanel);
	mainPanel.add(buttonJPane);
	
	mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
	
	
	 
	DepositMainFrame.add(mainPanel);
	DepositMainFrame.setVisible(true);
	DepositMainFrame.repaint();
	
	
	
	 class  Handler  implements ActionListener 
      {  
           
    	  public void actionPerformed(ActionEvent e)
    	  {
    		  
    		  if(e.getSource()==Deposit)
    		  {
    			  
    			  
    			  if(DepositFrame.Accounts.getSelectedItem() instanceof SavingAccount)
    			  {
 	    			  double sum = (double)Double.parseDouble(balance.getText());
 	    			  if(sum>SavingAccount.minimumDepositSum)
						try {
							((SavingAccount) DepositFrame.Accounts.getSelectedItem()).deposit(sum);
						} catch (Exception e1) {
 
							e1.printStackTrace();
						}
					else
 	    				  ErrorFrame.runErrorFrame("The introduced Sum is too small");
 	    				   
    			  }
    			  
    			  
    			  if(DepositFrame.Accounts.getSelectedItem() instanceof SpendingAccount)
    			  {
    				  
 	    			  double sum = (double)Double.parseDouble(balance.getText());
 	    			 ((SpendingAccount) DepositFrame.Accounts.getSelectedItem()).deposit(sum);
    				  
    			  }
    			  
    			  
    		   DepositMainFrame.dispose();
    		   AccountGUI.accountModel.setRowCount(0);
    		   App.banca.viewAllAccounts();
    		  }
    		 
    	  }
    		  
    }
	 
	 Handler handler = new Handler();
	 Deposit.addActionListener(handler);
	 
}
		
}
