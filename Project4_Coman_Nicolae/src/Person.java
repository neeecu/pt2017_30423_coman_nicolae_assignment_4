import java.util.Observable;
import java.util.Observer;

public class Person implements Observer,Comparable, java.io.Serializable{

	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private long personID;
		private String firstName;
		private String lastName;
		private String address;
		
	 
		
		public Person(long personID, String firstName, String lastName, String address)
		{
			this.personID=personID;
			this.firstName=firstName;
			this.lastName=lastName;
			this.address=address;
			
		}
		@Override
		public String toString() {
			return "Person [" + firstName + ", " + lastName + "]";
		}
		public Person(  String firstName, String lastName, String address)
		{
			 
			this.firstName=firstName;
			this.lastName=lastName;
			this.address=address;
			
		}
		
		public Person(){}
		
		public String getFirstName()
		{
			return this.firstName;
		}
		
		
		public String getLastName()
		{
			return this.lastName;
			
		}
		
		
		public String getAddresss()
		{
			
			return this.address;
		}
		
		
		public long getID()
		{
			return this.personID;
			
		}
		
		
		public void setFirstName(String firstName)
		{
			this.firstName=firstName;
			
		}
		
		
		public void setLastName(String lastName)
		{
			this.lastName=lastName;
			
		}
		
		
		public void setAddresss(String address)
		{
			
			this.address=address;
		}
		
		public void setID(long personID)
		{
			this.personID=personID;
		}

		
		
		 
		@Override
		public void update(Observable arg0, Object arg1) {
				System.out.println("Observed");
		}
		@Override
		public int compareTo(Object o) {
			int cmprnk = (int)((Person)o).getID();
			return (int)this.getID()- cmprnk;
		}
		
	 
}
