 
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
 
public class PersonGUI {
	public static JFrame mainFrame = new JFrame("Person");
	public static DefaultTableModel personModel= new DefaultTableModel(new Object[]{"ID","FirstName","LastName","Address"},0);

	
	
	
	
	public static void runPersonGUI() {
	
	
		
	mainFrame.setVisible(true);
	mainFrame.setSize(400,600);
	 
	mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	
    JButton showALL = new JButton("View all");
    JButton AddClient= new JButton("Add Person");
    JButton Exit = new JButton ("Exit");
    JTable intermediateTable = new JTable(PersonGUI.personModel);
    JTable personTable = new JTable();
   
    personTable.setEnabled(false);
    personTable.setSize(300,300);
    
    intermediateTable.addMouseListener(new MouseAdapter() {
    	  public void mousePressed(MouseEvent e) {
    	    if (e.getClickCount() == 1) {
    	      JTable target = (JTable)e.getSource();
    	      int row = target.getSelectedRow();
    	      
    	      JFrame newFrame = new JFrame();
    	     
    	      
    	      newFrame.setVisible(true);
    	      newFrame.setSize(400,400);
    	      mainFrame.setVisible(false);
    	      
    	      JLabel ID = new JLabel("ID");
    	      JLabel firstName = new JLabel("First Name");
    	      JLabel lastName = new JLabel("Last Name");
    	      JLabel Address = new JLabel("Address");
    	      
    	      JTextField tfID=new JTextField(), tffirstName=new JTextField(),tflastName=new JTextField(),tfAddress=new JTextField(); 
    	     
    	      JButton Edit  = new JButton ("    Edit    ");
    	      JButton Remove= new JButton ("Remove");
    	      JButton Back  = new JButton ("   Back   ");
    	      
    	      tfID.setText(String.valueOf(target.getValueAt(row, 0)));
    	      long PersonID=  (long) target.getValueAt(row, 0);
    	      tfID.setEditable(false);
    	      tfID.setColumns(5);
    	      
    	      tffirstName.setText((String)target.getValueAt(row,1));
    	      tffirstName.setColumns(5);
    	      
    	      tflastName.setText((String)target.getValueAt(row,2));
    	      tflastName.setColumns(5);
    	      
    	      tfAddress.setText((String)target.getValueAt(row,3));
    	      tfAddress.setColumns(5);
    	      
    	      JPanel boxJPanel1 = new JPanel();
    	      JPanel boxJPanel2 = new JPanel();
    	      BoxLayout boxlayout = new BoxLayout(boxJPanel1, BoxLayout.Y_AXIS);
    	      BoxLayout boxlayout2 = new BoxLayout(boxJPanel2, BoxLayout.Y_AXIS);
    	      boxJPanel1.setLayout(boxlayout);
    	      boxJPanel2.setLayout(boxlayout2);
    	      
    	      boxJPanel2.add(Edit);
    	      boxJPanel2.add(Remove);
    	      boxJPanel2.add(Back);
    	      
    	      JPanel p1 = new JPanel();
    	      p1.add(ID);
    	      p1.add(tfID);
    	      p1.setLayout(new FlowLayout());
    	      
    	      JPanel p2 = new JPanel();
    	      p2.add(firstName);
    	      p2.add(tffirstName);
    	      p2.setLayout(new FlowLayout());
    	      
    	      JPanel p3 = new JPanel();
    	      p3.add(lastName);
    	      p3.add(tflastName);
    	      p3.setLayout(new FlowLayout());
    	      
    	      JPanel p4 = new JPanel();
    	      p4.add(Address);
    	      p4.add(tfAddress);
    	      p4.setLayout(new FlowLayout());
    	      
    	      boxJPanel1.add(p1);
    	      boxJPanel1.add(p2);
    	      boxJPanel1.add(p3);
    	      boxJPanel1.add(p4);
    	      
    	      JPanel mainPanel = new JPanel(new FlowLayout());
    	      
    	      mainPanel.add(boxJPanel1);
    	      mainPanel.add(boxJPanel2);
    	      
    	      newFrame.add(mainPanel);
    	     
    	      newFrame.setVisible(true);
    	      
    	    
    	     
    	      class  Handler  implements ActionListener 
    	      {  
    	           
    	    	  public void actionPerformed(ActionEvent e){
    	    		  
    	    		  
    	    		  if(e.getSource()==Remove)
    	    		  {
    	    			  
    	    			  
    	    			  
    	    			  PersonGUI.personModel.setRowCount(0);
    	    			  App.banca.removePerson(PersonID);
    	    			  App.banca.viewAllPersons();
    	    			   
    	    			  intermediateTable.repaint();
    	    			  newFrame.dispose();
    	    			  mainFrame.setVisible(true);
    	    			  
    	    		  }
    	    		  
    	    		 if(e.getSource()==Edit)
    	    		 {
    	    			 long ID=(long)Integer.parseInt(tfID.getText());
    	    			 
    	    			 String firstName=tffirstName.getText();
   	    			     String lastName = tflastName.getText();
   	    			  	 String Address = tfAddress.getText();
    	    			 
   	    			  	 Person toWrite= new Person(firstName,lastName,Address);
   	    			  	 
   	    			  	 PersonGUI.personModel.setRowCount(0);
   	    			  	 App.banca.editPersonData((long)ID, toWrite);
   	    			  	 App.banca.viewAllPersons();
   	    			  	
   	    			  	 intermediateTable.repaint();
   	    			  	 newFrame.dispose();
	    			     mainFrame.setVisible(true);
    	    			 
    	    		 }
    	    		 if(e.getSource()==Back)
    	    		 {
    	    			 newFrame.dispose();
    	    			 mainFrame.setVisible(true);
    	    			 
    	    		 }
    	    		 
    	    	  }
    	      }
    	      Handler innerhandler=new Handler();
    	      Remove.addActionListener(innerhandler);
    	      Edit.addActionListener(innerhandler);
    	      Back.addActionListener(innerhandler);
    	    }
    	  }
    	});
    personTable=intermediateTable ;
    
    JPanel mainPanel = new JPanel(new BorderLayout());
    mainPanel.add(showALL,BorderLayout.NORTH);
    mainPanel.add(AddClient,BorderLayout.SOUTH);
    mainPanel.add(Exit,BorderLayout.EAST);
    mainPanel.add(new JScrollPane(personTable),BorderLayout.CENTER);
    mainFrame.add(mainPanel);
    mainFrame.setVisible(true);
    
    
    class  Handler  implements ActionListener 
      {  
           
    	  public void actionPerformed(ActionEvent e){
    		  
    		  
    		  if(e.getSource()==showALL)
    		  {
    			  
    			  PersonGUI.personModel.setRowCount(0);
    			  App.banca.viewAllPersons();

    			   
    			  
    			  
    		  }
    		  
    		  if(e.getSource()==AddClient)
    		  {	
    			  mainFrame.setVisible(false);
    			  NewPersonFrame.NewPersonFrame();
     			 
    			   
    		  }
    		  if(e.getSource()==Exit)
    		  {
    			  try {
    				  
    				    
    				    File file = new File("C:\\Users\\neecu\\workspace\\Project4_Coman_Nicolae\\banca.ser");
    				    file.createNewFile();
    				  	FileOutputStream fos = new FileOutputStream(file);
    					ObjectOutputStream oos = new ObjectOutputStream(fos);
    					oos.writeObject(App.banca);
    					oos.close();
    			       
    					System.out.println("Serialized data is saved in C:\\Users\\neecu\\workspace\\Project4_Coman_Nicolae\\banca.ser");
    			      }catch(IOException i) {
    			         i.printStackTrace();
    			      }
    			  mainFrame.dispose();
	    		  MainFrame.MainFrame.setVisible(true);

    		  }
    		  
    	  }
      }
    Handler handler= new Handler();
	showALL.addActionListener(handler);  
	AddClient.addActionListener(handler);
	Exit.addActionListener(handler);
	}	
}
