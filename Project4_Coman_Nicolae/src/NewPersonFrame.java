import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class NewPersonFrame {


	
	public static void NewPersonFrame()
	{
		
		JFrame mainFrame= new JFrame();
		mainFrame.setSize(400,400);
		JButton Add= new JButton("Add");
		
		JLabel ID = new JLabel("ID");
	    JLabel firstName = new JLabel("First Name");
	    JLabel lastName = new JLabel("Last Name");
	    JLabel Address = new JLabel("Address");
		
	    JTextField tfID=new JTextField(), tffirstName=new JTextField(),tflastName=new JTextField(),tfAddress=new JTextField(); 
	
	    tfID.setEditable(false);;
	    
	    
	    tfID.setColumns(5);
	    tffirstName.setColumns(5);
	    tflastName.setColumns(5);
	    tfAddress.setColumns(5);
	 
	    JPanel boxJPanel1 = new JPanel();
	    JPanel boxJPanel2 = new JPanel();
	    BoxLayout boxlayout = new BoxLayout(boxJPanel1, BoxLayout.Y_AXIS);
	    BoxLayout boxlayout2 = new BoxLayout(boxJPanel2, BoxLayout.Y_AXIS);
	
	    boxJPanel1.setLayout(boxlayout);
	    boxJPanel2.setLayout(boxlayout2);
	    
	    JPanel p1 = new JPanel();
	    p1.add(ID);
	    p1.add(tfID);
	    p1.setLayout(new FlowLayout());
	    
	    JPanel p2 = new JPanel();
	    p2.add(firstName);
	    p2.add(tffirstName);
	    p2.setLayout(new FlowLayout());
	    
	    JPanel p3 = new JPanel();
	    p3.add(lastName);
	    p3.add(tflastName);
	    p3.setLayout(new FlowLayout());
	      
	    JPanel p4 = new JPanel();
	    p4.add(Address);
	    p4.add(tfAddress);
	    p4.setLayout(new FlowLayout());
	      
	    boxJPanel1.add(p1);
	    boxJPanel1.add(p2);
	    boxJPanel1.add(p3);
	    boxJPanel1.add(p4);
	    
	    JPanel mainPanel = new JPanel(new FlowLayout());
	  
	    mainPanel.add(boxJPanel1);
	    boxJPanel2.add(Add);
	    mainPanel.add(boxJPanel2);
	    
	    mainFrame.add(mainPanel);
	    mainFrame.setVisible(true);
	      
	    class  Handler  implements ActionListener 
	      {  
	           
	    	  public void actionPerformed(ActionEvent e){
	    		  
	    		  
	    		  if(e.getSource()==Add)
	    		  {
	    			  
 	    			  
	    			  String firstName=tffirstName.getText();
	    			  String lastName = tflastName.getText();
	    			  String Address = tfAddress.getText();
	    			  
	    			  Person customer = new Person( firstName, lastName, Address);
	    			  
	    			  
	    			  PersonGUI.personModel.setRowCount(0);
	    			  App.banca.addPerson(customer);
	    			  mainFrame.dispose();
	    			  PersonGUI.mainFrame.setVisible(true);
	    			  App.banca.viewAllPersons();
	    			  
	    			  
	    		  }
	    	  }
	    	  
	
	      }
	   Handler handler = new Handler();
	   Add.addActionListener(handler);
}
}
