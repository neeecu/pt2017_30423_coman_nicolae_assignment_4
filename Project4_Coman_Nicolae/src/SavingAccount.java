//are dobana ; single deposit, single withrawn 
public class SavingAccount extends Account implements java.io.Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static final  double interestRate=0.05;
	 
	static final  double minimumDepositSum=100000.0;
	
	public SavingAccount(int ID, double Balance){
		super(ID,Balance);
	}
	
	public SavingAccount(double balance)
	{
		this.balance=balance;
	}
	
	public SavingAccount(){super();}
	
	public void withdraw()
	{
		
		balance=0;
		setChanged();
		notifyObservers();
	}
	
	public void deposit(double sum) throws Exception
	{
		
		if(sum<minimumDepositSum)
		{
			 throw new Exception("Entered sum is too small"); 
		}
		else
			 balance+=sum;
			 setChanged();
			 notifyObservers();
	}
	
	public void applyInterestRate()
	{
		
		balance+=balance*interestRate;
		setChanged();
		notifyObservers();
	
	}
	
	public String toString() {
		return "SavingAccount [" + balance + ", " + accountID + "]";
	}
	
 
}
