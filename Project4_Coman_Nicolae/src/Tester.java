import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Test;

@SuppressWarnings("restriction")
public class Tester {

	
	Bank test = new Bank();
	Person toAdd =new Person();


	
	@Test
	public void test1() {
		
		//testing the AddPersonMethod

		test.addPerson(toAdd);
		int noOfPersons=test.bankPersons.size();
		assertEquals(1,noOfPersons);
		
		
		
		
	}
	@Test
	public void test2()
	{
	
		test.addPerson(toAdd);
		
		//testing the AddAcountMethod
		Account accToAdd= new Account(1,650000.0);				
		test.addAcount(toAdd,accToAdd);
		Set <Account> receivedAcc =  test.table.get(toAdd);
		assertEquals(receivedAcc.contains(accToAdd), true);
				
		
		
	}
	
	@Test 
	public void test3() throws Exception{
	
	//testing the deposit /withdraw method
		Account addAcc = new Account(0.0);
		addAcc.deposit(100000.0);
		assertEquals(true,equals(100000.0,addAcc.getBalance()));
		
		
		
	}

	public boolean equals (double a, double b)
	{
		if(a==b)
			return true;
		else
			return false;
	}
	
	@Test
	public void test4() throws Exception{
		
		//test the withdraw method
		Account addAcc= new Account(100000.0);
		addAcc.withdraw(100000.0);
		assertEquals(true,equals(0.0,addAcc.getBalance()));
		
	}
	
	@Test public void test5(){
		
		//test the remove person method 
		test.table.remove(toAdd);
		Set <Person> testSet= test.table.keySet();
		
		assertEquals(0,testSet.size());
		
	}
	
	@Test public void test6()
	{
		//test the remove account method
		Account accToAdd=new Account(1,0.0);
		test.addAcount(toAdd, accToAdd);	
		
		int size1=test.table.get(toAdd).size();
		
		test.removeAccount(1);
		
		int size2= test.table.get(toAdd).size();
		assertEquals(size1,size2+1);
		
		
	}
	
	
	
}
