import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class WithdrawFrame {

	static JComboBox<Account> Accounts = new JComboBox<Account>();
	
	
	
	public static void RunWithdrawFrame()
	{
		JFrame WithdrawMainFrame = new JFrame();
		WithdrawMainFrame.setSize(300,300);
		
		JPanel mainPanel = new JPanel();
		
		
		
		
		
		JButton Withdraw = new JButton("Withdraw");
		JPanel buttonJPane = new JPanel();
		buttonJPane.setLayout(new FlowLayout());
		buttonJPane.add(Withdraw);
		
		
		JTextField balance = new JTextField();
		JLabel label1= new JLabel("Amount");
		balance.setColumns(10);
		
		Accounts.removeAllItems();
		App.banca.fillAccountComboBox(0);

		
		
		JPanel ComboBoxPanel = new JPanel();
		ComboBoxPanel.setLayout(new FlowLayout());
		
		JPanel balanceTextFieldPanel = new JPanel();
		balanceTextFieldPanel.setLayout(new FlowLayout());
		balanceTextFieldPanel.add(label1);
		balanceTextFieldPanel.add(balance);
		
		
		
		ComboBoxPanel.add(Accounts);
		mainPanel.add(ComboBoxPanel);
		mainPanel.add(balanceTextFieldPanel);
		mainPanel.add(buttonJPane);
		
		mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
		
		
		Accounts.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
               
 
                Object selected = Accounts.getSelectedItem();
            	if(selected instanceof SavingAccount)
            	{	balance.setEditable(false);
  			  		WithdrawMainFrame.repaint();	
  			  }
            	if(selected instanceof SpendingAccount)
  			  {	balance.setEditable(true);
  			  	WithdrawMainFrame.repaint();
   			  }
  		 

            }
        });
	
		WithdrawMainFrame.add(mainPanel);
		WithdrawMainFrame.setVisible(true);
		WithdrawMainFrame.repaint();
		
		
		
		 class  Handler  implements ActionListener 
	      {  
	           
	    	  public void actionPerformed(ActionEvent e)
	    	  {
	    		  
	    		  if(e.getSource()==Withdraw)
	    		  {
	    			  
	    			  
	    			  if(WithdrawFrame.Accounts.getSelectedItem() instanceof SavingAccount)
	    			  {
	    				  	
	    				
	    				   ((SavingAccount) WithdrawFrame.Accounts.getSelectedItem()).withdraw();
	    				  
	    			  }
	    			  
	    			  
	    			  if(WithdrawFrame.Accounts.getSelectedItem() instanceof SpendingAccount)
	    			  {
	    				  
	 	    			  double sum = (double)Double.parseDouble(balance.getText());
	 	    			  if(sum <=((SpendingAccount) WithdrawFrame.Accounts.getSelectedItem()).getBalance())
	 	    				  ((SpendingAccount) WithdrawFrame.Accounts.getSelectedItem()).withdraw(sum);
	 	    			  else
	 	    				  ErrorFrame.runErrorFrame("The sum introduced is larger than the current balance");
	    			  }
	    			  
	    			  
	    		   WithdrawMainFrame.dispose();
	    		   AccountGUI.accountModel.setRowCount(0);
	    		   App.banca.viewAllAccounts();
	    		   
	    		  }
	    		 
	    	  }
	    		  
	    }
		 
		 Handler handler = new Handler();
		 Withdraw.addActionListener(handler);
		 
	}

	
		
		
}

