import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

 
import javax.swing.JButton;
import javax.swing.JFrame;
 
import javax.swing.JPanel;
 

public class MainFrame {

	
	
	 public static JFrame MainFrame = new JFrame();

	public static void runGUI(){
		 MainFrame.setSize(300,100);
		 MainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
 		
		

		
		JButton Persons= new JButton("Person");
		JButton Accounts= new JButton("Accounts");
		JButton Report = new JButton("BankReport");
		JPanel buttonJPane = new JPanel();
		buttonJPane.setLayout(new FlowLayout());
		buttonJPane.add(Persons);
		buttonJPane.add(Accounts);
		buttonJPane.add(Report);
		
		
		
		 
		MainFrame.add(buttonJPane);
		MainFrame.pack();
		MainFrame.setVisible(true);
		MainFrame.repaint();
		
		App.banca=Bank.deserialize();

		
		
		 class  Handler  implements ActionListener 
	      {  
	           
	    	  public void actionPerformed(ActionEvent e)
	    	  {
	    		  
	    		  if(e.getSource()==Persons)
	    		  {
	    			  
	    			  MainFrame.setVisible(false);
	    			  PersonGUI.runPersonGUI();
	    			   
	    		 
	    	  }
	    		  
	    		  if(e.getSource()==Accounts)
	    		  {
	    			  
	    			  MainFrame.setVisible(false);
	    			  AccountGUI.runAccountGUI();
	    			   
	    		 
	    	  }
	    		  if(e.getSource()==Report)
	    			  
	    			 App.banca.generateReport();
	    			   
	    		 
	    	  }
	    		  
	    }
		 
		 Handler handler = new Handler();
		 Persons.addActionListener(handler);
		 Accounts.addActionListener(handler);
		 Report.addActionListener(handler);
		 
	}
}
