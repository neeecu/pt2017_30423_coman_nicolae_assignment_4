import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class AccountGUI {
	public static DefaultTableModel accountModel= new DefaultTableModel(new Object[]{"ID","Balance","Type","Owner"},0);
	public static JFrame mainFrame = new JFrame("Account");
	public static Vector<Account> lista = new Vector<Account>();
	
	public static void  runAccountGUI(){
		
		
		
		mainFrame.setVisible(true);
		mainFrame.setSize(400,600);
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		
	    JButton showALL = new JButton("View all");
	    JButton Back = new JButton("Exit");
	    JButton addAcount= new JButton("Add Acount");
	    
	    JButton withdraw  = new JButton ("Withdraw");
	    JButton deposit  =  new JButton ("Deposit");
	    JButton interestRate= new JButton("IR");
	    
	    JPanel moneyz = new JPanel();
	    moneyz.setLayout(new BoxLayout(moneyz, BoxLayout.Y_AXIS));
	    moneyz.add(withdraw);
	    moneyz.add(deposit);
	    moneyz.add(interestRate);
	    JTable intermediateTable = new JTable(AccountGUI.accountModel);
	    JTable personTable = new JTable();
	  
	   
	    personTable.setEnabled(false);
	    personTable.setSize(300,300);
	    
	    
	    intermediateTable.addMouseListener(new MouseAdapter() {
	    	  public void mousePressed(MouseEvent e) {
	    	    if (e.getClickCount() == 1) {
	    	      JTable target = (JTable)e.getSource();
	    	      int row = target.getSelectedRow();
	    	      
	    	      JFrame newFrame = new JFrame();
	    	     
	    	      
	    	      newFrame.setVisible(true);
	    	      newFrame.setSize(400,400);
	    	      mainFrame.setVisible(false);
	    	      
	    	      JLabel ID = new JLabel("ID");
	    	      JLabel Balance = new JLabel("Balance");
	    	      JLabel AccountType = new JLabel("Type");
	    	      JLabel AccountHolder = new JLabel("Holder");
	    	      
	    	      JTextField tfID=new JTextField(), tfBalance=new JTextField(),tfType=new JTextField(),tfHolder=new JTextField(); 
	    	     
	    	      JButton Edit  = new JButton ("    Edit    ");
	    	      JButton Remove= new JButton ("Remove");
	    	      JButton Back  = new JButton ("   Back   ");
	    	      
	    	      
	    	      tfID.setText(String.valueOf(target.getValueAt(row, 0)));
	    	      long accountID=  (long) target.getValueAt(row, 0);
	    	      tfID.setEditable(false);
	    	      tfID.setColumns(10);
	    	      
	    	      tfBalance.setText((String)target.getValueAt(row,1));
	    	      tfBalance.setColumns(10);
	    	      
	    	      tfType.setText((String)target.getValueAt(row,2));
	    	      tfType.setColumns(10);
	    	      
	    	      tfHolder.setText((String)target.getValueAt(row,3));
	    	      tfHolder.setEditable(false);
	    	      tfHolder.setColumns(10);
	    	      
	    	      JPanel boxJPanel1 = new JPanel();
	    	      JPanel boxJPanel2 = new JPanel();
	    	      BoxLayout boxlayout = new BoxLayout(boxJPanel1, BoxLayout.Y_AXIS);
	    	      BoxLayout boxlayout2 = new BoxLayout(boxJPanel2, BoxLayout.Y_AXIS);
	    	      boxJPanel1.setLayout(boxlayout);
	    	      boxJPanel2.setLayout(boxlayout2);
	    	      
	    	      boxJPanel2.add(Edit);
	    	      boxJPanel2.add(Remove);
	    	      boxJPanel2.add(Back);
	    	      
	    	      JPanel p1 = new JPanel();
	    	      p1.add(ID);
	    	      p1.add(tfID);
	    	      p1.setLayout(new FlowLayout());
	    	      
	    	      JPanel p2 = new JPanel();
	    	      p2.add(Balance);
	    	      p2.add(tfBalance);
	    	      p2.setLayout(new FlowLayout());
	    	      
	    	      JPanel p3 = new JPanel();
	    	      p3.add(AccountType);
	    	      p3.add(tfType);
	    	      p3.setLayout(new FlowLayout());
	    	      
	    	      JPanel p4 = new JPanel();
	    	      p4.add(AccountHolder);
	    	      p4.add(tfHolder);
	    	      p4.setLayout(new FlowLayout());
	    	      
	    	      boxJPanel1.add(p1);
	    	      boxJPanel1.add(p2);
	    	      boxJPanel1.add(p3);
	    	      boxJPanel1.add(p4);
	    	      
	    	      JPanel mainPanel = new JPanel(new FlowLayout());
	    	      
	    	      mainPanel.add(boxJPanel1);
	    	      mainPanel.add(boxJPanel2);
	    	      
	    	      newFrame.add(mainPanel);
	    	     
	    	      newFrame.setVisible(true);
	    	      
	    	    

	    	      class  Handler  implements ActionListener 
	    	      {  
	    	           
	    	    	  public void actionPerformed(ActionEvent e){
	    	    		  
	    	    		  
	    	    		  if(e.getSource()==Remove)
	    	    		  {
	    	    			  
	    	    			  AccountGUI.accountModel.setRowCount(0);
	    	    			  App.banca.removeAccount(accountID);
	    	    			  App.banca.viewAllAccounts();
	    	    			   
	    	    			  intermediateTable.repaint();
	    	    			  newFrame.dispose();
	    	    			  mainFrame.setVisible(true);
	    	    			  
	    	    			  
	    	    			  
	    	    		  }
	    	    		  
	    	    		 if(e.getSource()==Edit)
	    	    		 {
	    	    			 long ID=(long)Integer.parseInt(tfID.getText());
	    	    			 
	    	    			 double balance=(double)Double.parseDouble(tfBalance.getText());
	   	    			     String Type = tfType.getText();
	   	    			  	  
	    	    			 Account newAcc =null;
	   	    			  	 
	    	    			 try{
	   	    			  	 if(Type.equals("SavingAccount"))
	   	    			  		  newAcc= new SavingAccount((int)ID,balance);
	   	    			  	 else if(Type.equals("SpendingAccount"))
	   	    			  		  newAcc= new SpendingAccount((int)ID,balance);
	   	    			  	 else
	   	    			  		 throw new Exception("Account type entered is wront");
	    	    			 }catch(Exception e1)
	    	    			 {
	    	    				 e1.printStackTrace();
	    	    			 }
	    	    			 
	   	    			  	 AccountGUI.accountModel.setRowCount(0);
	   	    			  	 App.banca.editAccountData(ID, newAcc);
	   	    			  	 App.banca.viewAllAccounts();
	   	    			  	 intermediateTable.repaint();
	   	    			  	 newFrame.dispose();
		    			     mainFrame.setVisible(true);
	    	    			 
	    	    		 }
	    	    		 if(e.getSource()==Back)
	    	    		 {
	    	    			 newFrame.dispose();
	    	    			 mainFrame.setVisible(true);
	    	    			 
	    	    		 }
	    	    		 
	    	    	  }
	    	      }
	    	      Handler innerhandler=new Handler();
	    	      Remove.addActionListener(innerhandler);
	    	      Edit.addActionListener(innerhandler);
	    	      Back.addActionListener(innerhandler);
	    	      
	    	    }
	    	  }
	    	});
	    
	    
	    
	    personTable=intermediateTable ;
	    
	    JPanel mainPanel = new JPanel(new BorderLayout());
	    mainPanel.add(showALL,BorderLayout.NORTH);
	    mainPanel.add(Back,BorderLayout.EAST);
	    mainPanel.add(addAcount,BorderLayout.SOUTH);
	    mainPanel.add(moneyz,BorderLayout.WEST);
	    mainPanel.add(new JScrollPane(personTable),BorderLayout.CENTER);
	    mainFrame.add(mainPanel);
	    mainFrame.setVisible(true);
	    class  Handler  implements ActionListener 
	      {  
	           
	    	  public void actionPerformed(ActionEvent e){
	    		  
	    		  
	    		  if(e.getSource()==showALL)
	    		  {
	    			   
	    			  AccountGUI.accountModel.setRowCount(0);
	    			  App.banca.viewAllAccounts();
	    			 
	    			  intermediateTable.repaint();
	    			  
	    		  }
	    		  
	    		  if(e.getSource()==Back)
	    		  {
	    			  try {
	    				  
	    				    
	    				    File file = new File("C:\\Users\\neecu\\workspace\\Project4_Coman_Nicolae\\banca.ser");
	    				    file.createNewFile();
	    				  	FileOutputStream fos = new FileOutputStream(file);
	    					ObjectOutputStream oos = new ObjectOutputStream(fos);
	    					oos.writeObject(App.banca);
	    					oos.close();
	    			       
	    			         System.out.println("Serialized data is saved in C:\\Users\\neecu\\workspace\\Project4_Coman_Nicolae\\banca.ser");
	    			      }catch(IOException i) {
	    			         i.printStackTrace();
	    			      }
 	    			  mainFrame.dispose();
 	    			  MainFrame.MainFrame.setVisible(true);
	    		  }
	    		  
	    		  
	    		  if(e.getSource()==addAcount)
	    		  {
	    			  
	    			  NewAccountFrame.NewAccountFrame();
	    			  
	    		  }
	    		  
	    		  
	    		  if(e.getSource()==withdraw)
	    		  {
	    			  WithdrawFrame.RunWithdrawFrame();
 	    			  intermediateTable.repaint();
	    		  }
	    		  if(e.getSource()==deposit)
	    		  {
	    			  DepositFrame.RunDepositFrame();
	    			  intermediateTable.repaint();
	    		  }
	    		  
	    		  if(e.getSource()==interestRate)
	    		  {
	    			  
	    			  App.banca.fillList();
	    			  
	    			  for(Account a: lista)
	    			  {
	    				  if(a instanceof SavingAccount)
	    				  {
	    					  ((SavingAccount) a).applyInterestRate();
	    				  }
	    				  
	    			  }
	    			  AccountGUI.accountModel.setRowCount(0);

	    			  App.banca.viewAllAccounts();
	    			  intermediateTable.repaint();
	    		  }
	    		  
	    	  }
	      }
	    Handler handler= new Handler();
		showALL.addActionListener(handler); 
		Back.addActionListener(handler);
		addAcount.addActionListener(handler);
		withdraw.addActionListener(handler);
		deposit.addActionListener(handler);
		interestRate.addActionListener(handler);
	}
	
		
		
		
	}


